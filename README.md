# A simple comic book reader
A simple program to read comic book archives (.cb*)

To run, simply run it from the terminal with the path to the comic as the first
argument, e.g.

```
./reader.py comic.cbz
```

As of now, it only works with cbz files, but support for other formats is
planned.

## Dependencies
This program relies on Python 3.6 (or above) and pygobject

## Screenshots
![Saga #54](https://i.imgur.com/UNT6zGT.png)