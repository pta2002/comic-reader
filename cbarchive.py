import os
import zipfile
from gi.repository import GdkPixbuf


class ComicBook(object):
    pages = []

    def __init__(self, path):
        self.path = path
        _, ext = os.path.splitext(self.path)

        if ext == ".cbz":
            self.archivetype = "zip"
        elif ext == ".cba":
            self.archivetype = "ace"
        elif ext == ".cb7":
            self.archivetype = "7z"
        elif ext == ".cbr":
            self.archivetype = "rar"
        elif ext == ".cbt":
            self.archivetype = "tar"
        else:
            # TODO make custom exceptions
            raise Exception("Unsupported file type")
    
        self._load_pages()
    
    def _load_pages(self):
        # Attempt to load the comic file

        # Only supporting .zip for now
        # Later might support plugins for other types, since for example I can't
        # support rar without including proprietary code.
        if self.archivetype == "zip":
            archive = zipfile.ZipFile(self.path, 'r')
            nl = archive.namelist()
            # Should default to alphabetical order, which for lack of a
            # standard is the best we have
            nl.sort()
            for i, p in enumerate(nl):
                self.pages.append(ComicPage(archive, p, i))
        else:
            raise Exception("Unsupported file type")


class ComicPage(object):
    pixbuf = None

    def __init__(self, archive, filename, pagenum):
        self.archive  = archive
        self.filename = filename
        self.pagenum  = pagenum
    
    def get_pixbuf(self):
        # Generate a GdkPixBuf for the current page
        if self.pixbuf is None:
            pbloader = GdkPixbuf.PixbufLoader()

            with self.archive.open(self.filename) as file:
                pbloader.write(file.read())
            
            self.pixbuf = pbloader.get_pixbuf()
            pbloader.close()

        return self.pixbuf
