#!/bin/env python3
import gi
import sys

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository.GdkPixbuf import InterpType

from cbarchive import ComicBook

class Reader(Gtk.Window):
    curpage = 0
    def __init__(self, path):
        Gtk.Window.__init__(self, title="Reader")

        self.book = ComicBook(path)
        self.image = Gtk.Image()
        self.add(self.image)

        self.show_page()

        self.connect("key_press_event", self.on_key_press)
    
    def on_key_press(self, widget, event):
        key = Gdk.keyval_name(event.keyval)
        mpage = 0
        if key == "Left":
            mpage = -1
        if key == "Right":
            mpage = 1
        
        self.curpage = min(len(self.book.pages)-1, max(self.curpage+mpage, 0))
        self.show_page()
    
    def show_page(self):
        self.image.set_from_pixbuf(self.book.pages[self.curpage].get_pixbuf())

        p = self.image.get_pixbuf()
        target_h = 1000
        target_w = (target_h/p.get_height()) * p.get_width()

        self.image.set_from_pixbuf(p.scale_simple(target_w, target_h, InterpType.BILINEAR))

        self.set_title(f'Reader ({self.curpage+1}/{len(self.book.pages)})')
            

path = sys.argv[1]

win = Reader(path)
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()